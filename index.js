// console.log("Hello World");

const listArray = [];
    fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((data) => {
        data.map((elem) => {
            listArray.push(elem.title);
        });
    });
    console.log(listArray);



// number 5 - 6
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(json => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method  : 'GET',
   headers : {
      'Content-Type' : 'application/json'
   }
})
.then(response => response.json())
.then(json => console.log("The item " + json.title + " on the list has a status of " + json.completed));

// number 7
fetch('https://jsonplaceholder.typicode.com/todos', {
   method  : 'POST',
   headers : {
      'Content-Type' : 'application/json'
   },
   body: JSON.stringify({
      completed : false,
      title     : 'Created To Do List Item',
      userID    : 1
   })
})
.then(response => response.json())
.then(json => console.log(json));


// number 8
fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method  : 'PUT',
   headers : {
      'Content-Type' : 'application/json'
   },
   body: JSON.stringify({
      dateCompleted : 'Pending',
      description   : 'To update the my to do list with a different data structure',
      status        : 'Pending',
      title         : 'Updated To Do List Item',
      userID        : 1
   })
})
.then(response => response.json())
.then(json => console.log(json))

// number 10 - 11
fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method : 'PATCH',
   headers : {
      'Content-Type' : 'application/json'
   },
   body : JSON.stringify({
      dateCompleted : "07/11/21",
      status        : "Complete"
   })
})
.then(response => response.json())
.then(json => console.log(json));

// number 12
fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method : 'DELETE'
}) 


